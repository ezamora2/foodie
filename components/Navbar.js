import React from 'react';
import { StyleSheet, Pressable, Text, Image } from 'react-native';

const Navbar = ({}) =>{
    <View style={styles.menuFoodie}>
        <Pressable>  
            <Image
            style= {styles.iconMenu}
            source= {require('./assets/svg/Home.svg')}
            />
        </Pressable>
        <Pressable>   
            <Image
            style= {styles.iconMenu}
            source= {require('./assets/svg/Knife.svg')}
            />
        </Pressable>
        <Pressable> 
            <Image
            style= {styles.iconMenu}
            source= {require('./assets/svg/Plus.svg')}
            />
        </Pressable>
        <Pressable> 
            <Image
            style= {styles.iconMenu}
            source= {require('./assets/svg/Store.svg')}
            />
        </Pressable>
        <Pressable> 
            <Image
            style= {styles.iconMenu}
            source= {require('./assets/svg/User.svg')}
            />
        </Pressable>
        </View>
};


const styles = StyleSheet.create({
    menuFoodie:{
        marginTop: 110,
        width: 375,
        height: 83,
        backgroundColor: "#FAFAFA",
        flexWrap:"wrap",
        flexDirection: "row",
        borderTopWidth:1,
        justifyContent: "space-around",
        borderColor: "#E8E8E8",

    },
    iconMenu:{
        width: 30,
        height: 30,  
        marginTop: 18,
    },
});

export default Navbar;