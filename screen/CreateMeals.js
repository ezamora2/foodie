import React from 'react';
import { useState } from 'react';
import { StyleSheet, Text, View,Image,
        TouchableOpacity} from 'react-native';
import {InputField} from '../components';

const onPressCreateRestaurant = () => {
    console.log("Poner Direccion de la pantalla Login")
}
const onPressClose = () => {
    console.log("Cerrar pantalla")
}

const CreateMeal = () => {
    return (
        <View>
            <View style={styles.profileContainer}>      
                <View style={styles.redBlock}>
                    <TouchableOpacity onPress={onPressClose}>        
                            <Image
                            style= {styles.close}
                            source= {require('../assets/png/Close.png')}
                            />
                    </TouchableOpacity>
                    <Text style={styles.profile}>
                        Create Meal
                    </Text>
                    <Image
                        style= {styles.imageUser}
                        source= {require('../assets/png/User.png')}
                    />
                </View>
            </View>        
            <View style={styles.whiteBlockData}>
                <View style={styles.inputLogin}>
                    <View>
                        <InputField
                            inputStyle={{
                                fontSize: 16,
                                marginLeft: 5,
                            }}
                            containerStyle={{
                                alignContent: "center",
                                width:343,
                                height:50,
                                backgroundColor:'#F6F6F6',
                                borderRadius: 10,
                                marginHorizontal: 16,
                            }}
                            //leftIcon='name'
                            placeholder='Nombre del Platillo'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                    </View>
                    <View>
                        <InputField
                            inputStyle={{
                                fontSize: 16,
                                marginLeft: 5,
                            }}
                            containerStyle={{
                                alignContent: "center",
                                marginTop: 16, 
                                width:343,
                                height:50,
                                backgroundColor:'#F6F6F6',
                                borderRadius: 10,
                                marginHorizontal: 16,
                            }}
                            //leftIcon='name'
                            placeholder='Precio'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                    </View>
                    <View>
                        <InputField
                            inputStyle={{
                                fontSize: 16,
                                marginLeft: 5,
                            }}
                            containerStyle={{
                                alignContent: "center",
                                marginTop: 16, 
                                width:343,
                                height:50,
                                backgroundColor:'#F6F6F6',
                                borderRadius: 10,
                                marginHorizontal: 16,
                            }}
                            //leftIcon='name'
                            placeholder='Descripción'
                            autoCapitalize='none'
                            autoCorrect={false}
                        />
                    </View>
                </View>
                <View> 
                    <TouchableOpacity onPress={onPressCreateRestaurant} style={styles.button}>        
                        <Text style={styles.textButton}>Create</Text>
                    </TouchableOpacity>
                </View> 
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    profileContainer:{
        height: 286,
    },
    redBlock:{
        backgroundColor: '#EB5757',
        height: 245,
    },
    profile:{
        textAlign: "center",
        position: "absolute",
        alignSelf: "center",
        marginTop: 60,
        //fontFamily: "inner",
        fontStyle: "normal",
        fontSize: 30,
        lineHeight: 30,
        fontWeight: "600",
        height: 36,
        color: '#FFFFFF',
        
    },
    close:{
        alignSelf: "flex-start",
        width: 25,
        height: 25,
        marginTop: 60,
        marginLeft: 22, 
    }, 
    imageUser:{
        width:160,
        height:160,
        alignSelf:"center",
        marginTop: 127,
        position: "absolute", 
    },
    whiteBlockName:{
        backgroundColor: '#FFFFFF',
        height: 50,
        marginBottom: 24,
    },
    userName:{
        textAlign: "center",
        position: "absolute",
        alignSelf: "center",
        marginTop: 15,
        //fontFamily: "inner",
        fontStyle: "normal",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    whiteBlockData:{
        backgroundColor: '#FFFFFF',
        height: 64,
        display: "flex",
        alignItems: "center",
        borderBottomWidth: 2,
        borderColor: "#E8E8E8",
    },
    inputLogin:{
        flexDirection: "column",
        marginTop:35,
    },button: {
        marginTop: 50,
        marginHorizontal: 16,
        justifyContent: "center",
        alignItems:"center",
        overflow: "hidden",
        borderRadius: 25,
        backgroundColor: "#EB5757",
        height: 51,
        width: 343,
    },    
    textButton:{
        textAlign: "center",
        alignSelf:'center',
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#FFFFFF',
    },
});

export default CreateMeal;