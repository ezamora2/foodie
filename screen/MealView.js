import React from "react";
import { StyleSheet, Text, View, Pressable, Image, Button, TouchableHighlight } from "react-native";

const onPressEdit = () => {
  console.log("EDIT PRESSED")
}
const onPressDelete = () => {
  console.log("DELETE PRESSED")
}

const MealView = () => {
  return (
    <View style={[styles.container, {
      flexDirection: "column"
    }]}>
        <View style={[styles.containerRed] } >          
            <Pressable onPress={onPressEdit} >
                  <Text style={[styles.containerRedText] }>Edit</Text>
            </Pressable>
            <Text style={[styles.containerRedTitle]}>  Meal</Text>
            <Pressable onPress={onPressDelete} >
                  <Text style={[styles.containerRedText] }>Delete</Text>
            </Pressable>
        </View>
        <Image source={require('../img/Dishes/burgerIcon.png')} style={[styles.tinyLogo]} />
        <View style={[styles.containerWhite] } >
            <Text style={[styles.Oops] }>MealNameVariable</Text>
            <View style={[styles.mealInfo] }>
                <Image source={require('../img/Icons/ForkKnife.png')} style={[styles.icon]} />
                <Text style={[styles.textinfo]}>Description</Text>
            </View>
            <View style={[styles.mealInfo]}>
                <Image source={require('../img/Icons/Dollar.png')} style={[styles.icon]} />
                <Text style={[styles.textinfo, {fontWeight: "bold"}]}>Price</Text>
            </View>
            
        </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    textAlign: "center",
    fontFamily: "Inter",
  },
  containerRed: {
    height: 245,
    backgroundColor: "#EB5757",
    flexDirection: "row",
    justifyContent: "center",
  },
  containerRedText: {
    color: "#ffffff",
    marginTop: 68,
    fontSize: 16,
  },
  containerRedTitle: {
    color: "#ffffff",
    marginTop: 60,
    fontSize: 30,
    paddingLeft: 70,
    paddingRight: 70,    
  },
  tinyLogo: {
    width: 182,
    height: 182,
    marginTop: 127.37,
    alignSelf: "center",
    position: "absolute",    
  },
  containerWhite: {
    position: "absolute",
    paddingTop: 314,
    alignSelf: "center",
  },
  Oops: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: "bold",
  },
  mealInfo: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#E8E8E8",
    width: 343,
    top: 16,
  },
  icon: {
    width: 32,
    height: 32,
    margin: 10,
  },
  textinfo: {
      top: 14,
      left: 16,
      fontSize: 16,
  }
});

export default MealView;