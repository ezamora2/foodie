import React from 'react';
import { StyleSheet, Text, View, Image,TouchableOpacity, Pressable} from 'react-native';

const onPressLogIn = () => {
    console.log("Poner Direccion de la pantalla Login")
}

const onPressSignUp = () => {
    console.log("Poner Direccion de la pantalla Sign up")
}

const CreateAccount = () => {
    return (
        <View>  
            <View style={styles.redBlock}>
                <View style={styles.whiteBlockAlert}>
                    <Text style={styles.tittle}>
                        First things first!
                    </Text>
                    <Text style={styles.text}>
                        Please create an account in order to register a restaurant or a meal.
                    </Text>
                    <TouchableOpacity style={styles.buttonCreate} onPress={onPressSignUp}>
                    <Text style={styles.textWhite}>
                        Create an account
                    </Text>
                    </TouchableOpacity>
                    <Pressable onPress={onPressLogIn}>
                        <Text style={styles.login}>
                            Login
                        </Text>
                    </Pressable>
                </View>
            </View>
            <View style={styles.menuFoodie}>
                <Image
                style= {styles.iconMenu}
                source= {require('../assets/svg/Home.svg')}
                />
                <Image
                style= {styles.iconMenu}
                source= {require('../assets/svg/Knife.svg')}
                />
                <Image
                style= {styles.iconMenu}
                source= {require('../assets/svg/Plus.svg')}
                />
                <Image
                style= {styles.iconMenu}
                source= {require('../assets/svg/Store.svg')}
                />
                <Image
                style= {styles.iconMenu}
                source= {require('../assets/svg/User.svg')}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    redBlock:{
        backgroundColor: '#EB5757',
        height: 725,
        justifyContent: "center",
        alignContent:"center",
    },
    whiteBlockAlert:{
        backgroundColor: '#FFFFFF',
        marginLeft: 16,
        marginRight: 16,
        height: 256,
        borderRadius: 8,
        alignItems: "center",
    },
    tittle:{
        textAlign: "center",
        position: "absolute",
        alignSelf: "center",
        marginTop: 32,
        marginHorizontal: 51,
        //fontFamily: "inner",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    text:{
        textAlign: "center",
        marginHorizontal: 16,
        marginTop: 84,
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#666666',
    },
    buttonCreate:{
        justifyContent:'center',
        marginTop: 16,
        width:311,
        height:52,
        backgroundColor:'#EB5757',
        borderRadius:25,
    },
    textWhite:{
        textAlign: "center",
        alignSelf:'center',
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#FFFFFF',
    },
    login:{
        textAlign: "center",
        alignSelf:'center',
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#EB5757',
        marginTop: 16,
    },
    menuFoodie:{
        width: 375,
        height: 83,
        backgroundColor: "#FAFAFA",
        flexWrap:"wrap",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    iconMenu:{
        width: 30,
        height: 30,  
        marginTop: 18,
    },
});

export default CreateAccount;