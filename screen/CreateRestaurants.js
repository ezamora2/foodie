import React from 'react';
import { useState } from 'react';
import { StyleSheet, Text, View,Image,
        TouchableOpacity} from 'react-native';
import {InputField} from '../components';

const onPressCreateRestaurant = () => {
    console.log("Poner Direccion de la pantalla Login")
}
const onPressClose = () => {
    console.log("Cerrar pantalla")
}
const RestaurantCreate = () => {
    return (
        <View style={styles.whiteBlock}>
            <View>
                <TouchableOpacity onPress={onPressClose}>        
                    <Image
                    style= {styles.close}
                    source= {require('../assets/png/Close.png')}
                    />
                </TouchableOpacity>
                <Text style={styles.tittle}>
                    New Restaurant
                </Text>
            </View>
            <View style={styles.inputLogin}>
                <View>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            alignContent: "center",
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        leftIcon='name'
                        placeholder='Nombre Restaurante'
                        autoCapitalize='none'
                        autoCorrect={false}
                    />
                </View>
                <View>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            alignContent: "center",
                            marginTop: 16, 
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        //leftIcon='name'
                        placeholder='Dirección'
                        autoCapitalize='none'
                        autoCorrect={true}
                    />
                </View>
                <View>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            alignContent: "center",
                            marginTop: 16, 
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        //leftIcon='name'
                        placeholder='Municipio'
                        autoCapitalize='none'
                        autoCorrect={true}
                    />
                </View>
                <View>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            alignContent: "center",
                            marginTop: 16, 
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        //leftIcon='name'
                        placeholder='Categoria'
                        autoCapitalize='none'
                        autoCorrect={true}
                    />
                </View>
                <View>
                    <InputField
                        inputStyle={{
                            fontSize: 16,
                            marginLeft: 5,
                        }}
                        containerStyle={{
                            alignContent: "center",
                            marginTop: 16, 
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius: 10,
                            marginHorizontal: 16,
                        }}
                        //leftIcon='name'
                        placeholder='RFC'
                        autoCapitalize='none'
                        autoCorrect={false}
                    />
                </View>
            </View>
            <View> 
                <TouchableOpacity onPress={onPressCreateRestaurant} style={styles.button}>        
                    <Text style={styles.textButton}>Create</Text>
                </TouchableOpacity>
            </View> 
        </View>
    );
}

const styles = StyleSheet.create({
    whiteBlock:{
        backgroundColor: '#FFFFFF',
        height: 812,
    },
    tittle:{
        textAlign: "center",
        alignSelf: "center",
        position: "absolute",
        marginTop: 32,
        //fontFamily: "inner",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    inputLogin:{
        flexDirection: "column",
        marginTop:35,
    },
    close:{
        alignSelf: "flex-start",
        width: 25,
        height: 25,
        marginTop: 34,
        marginLeft: 22, 
    },  
    button: {
        marginTop: 50,
        marginHorizontal: 16,
        justifyContent: "center",
        alignItems:"center",
        overflow: "hidden",
        borderRadius: 25,
        backgroundColor: "#EB5757",
        height: 51,
        width: 343,
    },    
    textButton:{
        textAlign: "center",
        alignSelf:'center',
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#FFFFFF',
    },
});

export default RestaurantCreate;
