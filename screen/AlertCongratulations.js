import React from "react";
import { StyleSheet , View, Text, TouchableHighlight} from 'react-native';

const onContinue = () => {
  console.log("ADD ACCION FOR CONTINUING")
}

const AlertCongratulations = () => {
    return (
        <View style={[styles.container, {
            flexDirection: "column"
          }]}> 
          
          <View style={[styles.theAlert]}>
          <Text style={[styles.Oops] }>Congratulations!</Text>
          <Text style={[styles.message] }>Your restaurant/Meal was successfully {"\n"} created.</Text>

            
              <TouchableHighlight onPress={onContinue} style={[styles.btn]}>        
                <Text style={[styles.btnText] }>Continue</Text>
              </TouchableHighlight>

          </View>
          
        </View>
    );
 };

 const styles = StyleSheet.create({
    container: {
      flex: 1,
      textAlign: "center",
      fontFamily: "Inter",
      backgroundColor: "#EB5757",
    },
    theAlert: {
      position: "absolute",
      width: 343,
      height: 221,
      top: 296,
      backgroundColor: "#ffffff",
      alignSelf: "center",
      borderRadius: 8,
    },
    Oops: {
      fontSize: 30,
      textAlign: "center",
      fontWeight: "bold",
      top: 32,
    },
    message: {
      fontSize: 16,
      textAlign: "center",
      color: "#666666",
      top: 45,
    },
    btn: {
      overflow: "hidden",
      borderRadius: 100,
      backgroundColor: "#EB5757",
      height: 51,
      width: 311,
      alignSelf: "center",
      top: 60,
    },
    btnText: {
      color: "white",
      textAlign: "center",
      fontSize: 16,
      paddingTop: 16,      
    },
  });
  
  export default AlertCongratulations;