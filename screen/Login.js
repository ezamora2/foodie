import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useState } from 'react';
import { StyleSheet, Text, View,Image,
    TouchableOpacity, Pressable, SafeAreaView, TextInput} from 'react-native';
    import {InputField} from '../components';


const onPressSignUp = () => {
    console.log("Poner Direccion de la pantalla Sign up")
}
const onPressClose = () => {
    console.log("Cerrar pantalla")
}

const LogIn = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [rightIcon, setRightIcon] = useState('eye');
    const [passwordVisibility, setPasswordVisibility] = useState(true);

    const handlePasswordVisibility = () => {
        if (rightIcon === 'eye') {
            setRightIcon('eye-off');
            setPasswordVisibility(!passwordVisibility);
        } else if (rightIcon === 'eye-off') {
            setRightIcon('eye');
            setPasswordVisibility(!passwordVisibility);
        }
    };

    const onPressLogIn = async () => {
        console.log(email, password)
        const res = await fetch('http://localhost:3000/users/signin', {
                    method: "post",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body:  JSON.stringify({
                      email: email,
                      password: password
                    })                
                })
                const user = await res.json()
                console.log(user)
                console.log(user[0].id)
                await AsyncStorage.setItem('ID', user[0].id)
                const value = await AsyncStorage.getItem('ID')
                console.log(value)

    }

    return (
        <View style={styles.whiteBlock}>
            <View>
                <TouchableOpacity onPress={onPressClose}>        
                    <Image
                    style= {styles.close}
                    source= {require('../assets/png/Close.png')}
                    />
                </TouchableOpacity>
                <Text style={styles.tittle}>
                    Login
                </Text>
            </View>
            <View style={styles.inputLogin}>
                <SafeAreaView>
                    <InputField
                        inputStyle={{
                        fontSize: 16
                        }}
                        containerStyle={{
                            marginTop: 16,
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius:5,
                            marginHorizontal: 16,
                        }}
                        leftIcon='email'
                        placeholder='Correo electrónico'
                        autoCapitalize='none'
                        keyboardType='email-address'
                        textContentType='emailAddress'
                        autoFocus={true}
                        value={email}
                        onChangeText={text => setEmail(text)}
                    />
                    <InputField
                        inputStyle={{
                        fontSize: 16
                        }}
                        containerStyle={{
                            marginTop: 16,
                            width:343,
                            height:50,
                            backgroundColor:'#F6F6F6',
                            borderRadius:5,
                            marginHorizontal: 16,
                        }}
                        leftIcon='lock'
                        placeholder='Contraseña'
                        autoCapitalize='none'
                        autoCorrect={false}
                        secureTextEntry={passwordVisibility}
                        textContentType='password'
                        rightIcon={rightIcon}
                        value={password}
                        onChangeText={text => setPassword(text)}
                        handlePasswordVisibility={handlePasswordVisibility}
                    />
                </SafeAreaView>
            </View>
            <View>
                <TouchableOpacity onPress={onPressLogIn} style={[styles.btn]}>        
                    <Text style={styles.logIn}>Log In</Text>
                </TouchableOpacity>
                <Pressable onPress={onPressSignUp} >
                    <Text style={styles.lastPressable}>Don’t have and account ? Sign Up!</Text>
                </Pressable>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    whiteBlock:{
        backgroundColor: '#FFFFFF',
        height: 812,
    },
    tittle:{
        textAlign: "center",
        alignSelf: "center",
        position: "absolute",
        marginTop: 32,
        //fontFamily: "inner",
        fontSize: 30,
        lineHeight: 36,
        fontWeight: "600",
        color: '#000000',
    },
    close:{
        alignSelf: "flex-start",
        width: 25,
        height: 25,
        marginTop: 34,
        marginLeft: 22, 
    },
    text:{
        textAlign: "center",
        marginHorizontal: 16,
        marginTop: 84,
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#666666',
    },
    inputLogin:{
        marginTop:20,
    },
    logIn:{
        textAlign: "center",
        alignSelf:'center',
        //fontFamily: "inner",
        fontSize: 16,
        lineHeight: 19,
        fontWeight: "normal",
        color: '#FFFFFF',
    },
    btn: {
        marginTop: 70,
        marginHorizontal: 16,
        justifyContent: "center",
        overflow: "hidden",
        borderRadius: 25,
        backgroundColor: "#EB5757",
        height: 51,
        width: 343,
    },
    lastPressable: {
        paddingTop: 16,
        fontSize: 16,
        textAlign: "center",
        color: "#EB5757",
        fontWeight: "bold",
    },
});

export default LogIn;



